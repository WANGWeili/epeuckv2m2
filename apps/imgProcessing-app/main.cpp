//#include <epuck.h>
//#include <controlFunctions.h>
#include "RobotDriver.hpp"

/*****************************/
/**** Main Program ***********/
/*****************************/

void IncorrectArguments(const int& argc);

int main(int argc, char** argv) {
    if (argc != 2) {
        IncorrectArguments(argc);
    }
    std::string nameOfReadFolder = argv[1];
    int cnt = 1;
    Pose curPoseFromEnc, curPoseFromVis, initPose;
    std::vector<double> xEnc, yEnc, thEnc;
    xEnc = loadSensorLogs(nameOfReadFolder + "data/xEnc.txt");
    yEnc = loadSensorLogs(nameOfReadFolder + "data/yEnc.txt");
    thEnc = loadSensorLogs(nameOfReadFolder + "data/thEnc.txt");

    //std::string nameOfWriteFolder = createLogFolder();
    Logger logger;
    cv::Mat colImgFromDisk;
    initPose.setPose(.32, 0., M_PI);
    RobotParameters rp;
    while(true){
        std::cout << COLOR_COUT_BLUE_BRIGHT;//write in bold cyan
        std::cout << "\nSTART ITERATION " << cnt <<" \n";
        std::cout << COLOR_COUT_RESET;//reset color

        if(cnt == 1) {
            curPoseFromEnc = initPose;
            curPoseFromVis = initPose;
            colImgFromDisk = loadAndShowImageFromDisk(nameOfReadFolder, 2);
        } else {
            curPoseFromEnc.x = xEnc[cnt-1];
            curPoseFromEnc.y = yEnc[cnt-1];
            curPoseFromEnc.th = thEnc[cnt-1];
            colImgFromDisk = loadAndShowImageFromDisk(nameOfReadFolder, cnt); 
        }
        float areaPix;
        cv::Point baryc = processImageToGetBarycenter(colImgFromDisk, areaPix);
        float vel, omega;
        struct timeval startTime;
        controlRobotWithVisualServoing(baryc, vel, omega, areaPix);
        curPoseFromVis = getCurrPoseFromVision(baryc, curPoseFromEnc.th, areaPix, logger);

        cv::Point2f ProxInWFrame[10];
        float mWorld, pWorld;
        drawMapWithRobot(rp, curPoseFromEnc, curPoseFromVis, ProxInWFrame, mWorld, pWorld);

        cv::waitKey(0);
        cnt++;
    }
    return 0;
}


void IncorrectArguments(const int& argc) {
    printf("There are %d arguments instead of 1\n", argc - 1);
    printf("The argument should be the path to the folder containing the images to be processed \n");
    exit(0);
}
